package com.task.service;

import com.task.entity.MeteoData;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;

@Component
public class MeteoDataService {
	@PersistenceContext
	private EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Transactional
	public void register(MeteoData meteoData) {
		this.em.persist(meteoData);
	}

	@Transactional
	public void update(MeteoData meteoData) {
		this.em.merge(meteoData);
	}

	public Collection<MeteoData> getAllMeteoDataAccuWeather() {
		Query query = em.createQuery("SELECT m FROM MeteoData m Where m.city = 'Moscow' or m.city = 'Perm' or m.city = 'Yekaterinburg'");
		return (Collection<MeteoData>) query.getResultList();
	}

	public Collection<MeteoData> getAllMeteoDataGisMeteo() {
		Query query = em.createQuery("SELECT m FROM MeteoData m Where m.city = 'Москва' or m.city = 'Пермь' or m.city = 'Екатеринбург'");
		return (Collection<MeteoData>) query.getResultList();
	}

	@Transactional
	public void removeAllDataAccuWeather() {
    	Query query = em.createQuery("DELETE FROM MeteoData m WHERE m.city = 'Moscow' or m.city = 'Perm' or m.city = 'Yekaterinburg'");
		query.executeUpdate();
	}

	@Transactional
	public void removeAllDataGisMeteo() {
		Query query = em.createQuery("DELETE FROM MeteoData m WHERE m.city = 'Москва' or m.city = 'Пермь' or m.city = 'Екатеринбург'");
		query.executeUpdate();
	}
}
