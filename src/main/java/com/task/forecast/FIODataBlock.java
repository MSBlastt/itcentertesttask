package com.task.forecast;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

public class FIODataBlock {

	private String summary;
	private String icon;
	private FIODataPoint [] data;
	private String timezone;

	public FIODataBlock(){
		this.summary = "";
		this.icon = "";
		this.data = null;
		this.timezone = "GMT";
	}

	public FIODataBlock(JsonObject db){
		this.summary = "";
		this.icon = "";
		this.data = null;
		this.timezone = "GMT";
		update(db);
	}

	private void update(JsonObject db){

		try {
			this.summary = db.get("summary").asString();
		} catch (NullPointerException npe) {
			this.summary = "no data";
		}

		try {
			this.icon = db.get("icon").asString();
		} catch (NullPointerException npl) {
			this.icon = "no data";
		}

		try {
			JsonArray arr;
			if(db.get("data").isArray()){
				arr = db.get("data").asArray();
				this.data = new FIODataPoint[arr.size()];
				for(int i = 0; i < arr.size(); i++){
					this.data[i] = new FIODataPoint();
					this.data[i].setTimezone(timezone);
					data[i].update(arr.get(i).asObject());
				}
			}
			else {
				System.err.println("Not an array. Maybe you're trying to feed \"currently\" to a datablock.");
			}
		} catch (NullPointerException mpe) {
			this.data = null;
		}
	}

	public String summary(){
		return this.summary;
	}

	public String icon(){
		return this.icon;
	}

	public FIODataPoint datapoint(int index){
		return this.data[index];
	}

	public int datablockSize(){
		return this.data.length;
	}

	public void setTimezone(String tz){
		this.timezone = tz;
	}

	public String getTimezone(){
		return this.timezone;
	}

}
