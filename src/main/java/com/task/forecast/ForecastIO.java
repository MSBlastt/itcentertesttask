package com.task.forecast;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

public class ForecastIO {


	private static final String ForecastIOURL = "https://api.forecast.io/forecast/";
	private String ForecastIOApiKey = "";
	private String unitsURL;
	private String timeURL;
	private String excludeURL;
	private String langURL;
	private boolean extend;

	private String Cache_Control;
	private String Expires;
	private String X_Forecast_API_Calls;
	private String X_Response_Time;
	
	private String rawResponse;


	public static final String UNITS_US = "us";
	public static final String UNITS_SI = "si";
	public static final String UNITS_CA = "ca";
	public static final String UNITS_UK = "uk";
	public static final String UNITS_AUTO = "auto";

	public static final String LANG_BOSNIAN = "bs";
	public static final String LANG_GERMAN = "de";
	public static final String LANG_ENGLISH = "en";
	public static final String LANG_SPANISH = "es";
	public static final String LANG_FRENCH = "fr";
	public static final String LANG_ITALIAN = "it";
	public static final String LANG_DUTCH = "nl";
	public static final String LANG_POLISH = "pl";
	public static final String LANG_PORTUGUESE = "pt";
	public static final String LANG_TETUM = "tet";
	public static final String LANG_X_PG_LATIN = "Igpay";


	private JsonObject forecast;

	private JsonObject currently;
	private JsonObject minutely;
	private JsonObject hourly;
	private JsonObject daily;
	private JsonObject flags;
	private JsonArray alerts;

	public ForecastIO(String API_KEY){	

		if (API_KEY.length()==32) {
			this.ForecastIOApiKey = API_KEY;
			this.forecast = new JsonObject();
			this.currently = new JsonObject();
			this.minutely = new JsonObject();
			this.hourly = new JsonObject();
			this.daily = new JsonObject();
			this.flags = new JsonObject();
			this.alerts = new JsonArray();
			this.timeURL = null;
			this.excludeURL = null;
			this.extend = false;
			this.unitsURL = UNITS_AUTO;
			this.langURL = LANG_ENGLISH;
		}
		else {
			System.err.println("The API Key doesn't seam to be valid.");
		}
	}//construtor - end

	public ForecastIO(String LATITUDE, String LONGITUDE, String API_KEY){	

		if (API_KEY.length()==32) {
			this.ForecastIOApiKey = API_KEY;
			this.forecast = new JsonObject();
			this.currently = new JsonObject();
			this.minutely = new JsonObject();
			this.hourly = new JsonObject();
			this.daily = new JsonObject();
			this.flags = new JsonObject();
			this.alerts = new JsonArray();
			this.timeURL = null;
			this.excludeURL = null;
			this.extend = false;
			this.unitsURL = UNITS_AUTO;
			this.langURL = LANG_ENGLISH;

			getForecast(LATITUDE, LONGITUDE);
		}
		else {
			System.err.println("The API Key doesn't seam to be valid.");
		}

	}//construtor - end

	public ForecastIO(String LATITUDE, String LONGITUDE, String UNITS, String LANG, String API_KEY){	

		if (API_KEY.length()==32) {
			this.ForecastIOApiKey = API_KEY;
			this.forecast = new JsonObject();
			this.currently = new JsonObject();
			this.minutely = new JsonObject();
			this.hourly = new JsonObject();
			this.daily = new JsonObject();
			this.flags = new JsonObject();
			this.alerts = new JsonArray();
			this.timeURL = null;
			this.excludeURL = null;
			this.extend = false;
			this.setUnits(UNITS);
			this.setLang(LANG);
			getForecast(LATITUDE, LONGITUDE);
		} else {
			System.err.println("The API Key doesn't seam to be valid.");
		}

	}//construtor - end


	public Double getLatitude(){
		return this.forecast.get("latitude").asDouble();
	}

	public Double getLongitude(){
		return this.forecast.get("longitude").asDouble();
	}


	public String getTimezone(){
		return this.forecast.get("timezone").asString();
	}

	public String getTime() {
		return timeURL;
	}

	public void setTime(String time) {
		this.timeURL = time;
	}

	public String getExcludeURL() {
		return excludeURL;
	}

	public void setExcludeURL(String excludeURL) {
		this.excludeURL = excludeURL;
	}

	public boolean isExtend() {
		return extend;
	}

	public void setExtend(boolean extend) {
		this.extend = extend;
	}

	public int offsetValue(){
		return this.forecast.get("offset").asInt();
	}

	public String offset(){
		if(this.forecast.get("offset").asInt()<0)
			return ""+"-"+this.forecast.get("offset").asInt();
		else if(this.forecast.get("offset").asInt()>0)
			return ""+"+"+this.forecast.get("offset").asInt();
		else
			return "";
	}

	public String getUnits(){
		return this.unitsURL;
	}

	public void setUnits(String units){
		this.unitsURL = units;
	}

	public String getLang(){
		return this.langURL;
	}

	public void setLang(String lang){
		this.langURL = lang;

	}

	public JsonObject getCurrently(){
		return this.currently;
	}

	public JsonObject getMinutely(){
		return this.minutely;
	}

	public JsonObject getHourly(){
		return this.hourly;
	}

	public JsonObject getFlags(){
		return this.flags;
	}

	public JsonArray getAlerts(){
		return this.alerts;
	}

	public JsonObject getDaily(){
		return this.daily;
	}

	public boolean hasCurrently(){
		return this.currently != null;
	}

	public boolean hasMinutely(){
		return this.minutely != null;
	}

	public boolean hasHourly(){
		return this.hourly != null;
	}

	public boolean hasDaily(){
		return this.daily != null;
	}

	public boolean hasFlags(){
		return this.flags != null;
	}

	public boolean hasAlerts(){
		return this.alerts != null;
	}

	private String urlBuilder(String LATITUDE, String LONGITUDE){
		StringBuilder url = new StringBuilder("");
		url.append(ForecastIOURL);
		url.append(ForecastIOApiKey).append("/");
		url.append(LATITUDE).append(",").append(LONGITUDE);
		if(timeURL!=null)
			url.append(",").append(timeURL);
		url.append("?units=").append(unitsURL);
		url.append("&lang=").append(langURL);
		if(excludeURL!=null)
			url.append("&exclude=").append(excludeURL);
		if(extend)
			url.append("&extend=hourly");
		return url.toString();
	}

	public boolean update(){
		boolean b = getForecast(String.valueOf(getLatitude()), String.valueOf(getLongitude()));
		return b;
	}

	public boolean getForecast(String LATITUDE, String LONGITUDE) {


		try {
			String reply = httpGET( urlBuilder(LATITUDE, LONGITUDE) );
			if(reply == null)
				return false;
			this.forecast = JsonObject.readFrom(reply);
		} catch (NullPointerException e) {
			System.err.println("Unable to connect to the API: "+e.getMessage());
			return false;
		}

		return getForecast(this.forecast);


	}//getForecast - end

	public boolean getForecast(String http_response) {

		this.forecast = JsonObject.readFrom(http_response);
		return getForecast(this.forecast);
	}


	public boolean getForecast(JsonObject forecast) {
		this.forecast = forecast;
		try {
			this.currently = forecast.get("currently").asObject();
		} catch (NullPointerException e) {
			this.currently = null;
		}
		try {
			this.minutely = forecast.get("minutely").asObject();
		} catch (NullPointerException e) {
			this.minutely = null;
		}
		try {
			this.hourly = forecast.get("hourly").asObject();
		} catch (NullPointerException e) {
			this.hourly = null;
		}
		try {
			this.daily = forecast.get("daily").asObject();
		} catch (NullPointerException e) {
			this.daily = null;
		}
		try {
			this.flags = forecast.get("flags").asObject();
		} catch (NullPointerException e) {
			this.flags = null;
		}
		try {
			this.alerts = forecast.get("alerts").asArray();
		} catch (NullPointerException e) {
			this.alerts = null;
		}

		return true;
	}//getForecast - end

	public String getUrl(String LATITUDE, String LONGITUDE) {
		return urlBuilder(LATITUDE, LONGITUDE);
	}

	public String getHeaderCache_Control() {
		return Cache_Control;
	}

	public String getHeaderExpires() {
		return Expires;
	}

	public String getHeaderX_Forecast_API_Calls() {
		return X_Forecast_API_Calls;
	}

	public String getHeaderX_Response_Time() {
		return X_Response_Time;
	}

	public String getRawResponse() {
		return rawResponse;
	}

	private String httpGET(String requestURL) {

		//Variables
		URL request = null;
		HttpURLConnection connection = null;
		//Scanner scanner = null;
		BufferedReader reader = null;
		String s = "";
		String response = "";

		try {
			request = new URL(requestURL);
			connection = (HttpURLConnection) request.openConnection();

			connection.setRequestMethod("GET");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(false);
			connection.setRequestProperty("Accept-Encoding", "gzip");
			connection.connect();

			Cache_Control = connection.getHeaderField("Cache-Control");
			Expires = connection.getHeaderField("Expires");
			X_Forecast_API_Calls = connection.getHeaderField("X-Forecast-API-Calls");
			X_Response_Time = connection.getHeaderField("X-Response-Time");

			if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){

				try {
					if(connection.getRequestProperty("Accept-Encoding") != null){

						reader = new BufferedReader(new InputStreamReader( new GZIPInputStream( connection.getInputStream() )));
						while( (s = reader.readLine()) != null )
							response = s;

					} else {

						reader = new BufferedReader(new InputStreamReader( connection.getInputStream() ));
						while( (s = reader.readLine()) != null )
							response = s;

					}
				} catch (IOException e){
					System.err.println("Error: "+e.getMessage());
				} finally {
					if (reader != null) {
						try {
							reader.close();
							reader = null;
						} catch (IOException e) {
							System.err.println("Error: "+e.getMessage());
						}
					}
				}

			} //if HTTP_OK - End
			// else if HttpURLConnection Not Ok
			else {

				try {
					reader = new BufferedReader(new InputStreamReader( connection.getErrorStream() ));
					while( (s = reader.readLine()) != null )
						response = s;
				} catch (IOException e){
					System.err.println("Error: "+e.getMessage());
				} finally {
					if (reader != null) {
						try {
							reader.close();
							reader = null;
						} catch (IOException e) {
							System.err.println("Error: "+e.getMessage());
						}
					}
				}
				//If response is not ok print error and return null
				System.err.println("Bad Response: " + response + "\n");
				return null;

			} //else if HttpURLConnection Not Ok - End
		} catch (IOException e) {
			System.err.println("Error: "+e.getMessage());		
			response = null;
		} finally {
			assert connection != null;
			connection.disconnect();
		}
		
		rawResponse = response;
		return response;
	}//httpGET - end

}//public class - end

