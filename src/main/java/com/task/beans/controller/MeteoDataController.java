package com.task.beans.controller;

import com.task.entity.MeteoData;
import com.task.forecast.FIOApiKey;
import com.task.forecast.FIOCurrently;
import com.task.forecast.ForecastIO;
import com.task.service.MeteoDataService;
import org.primefaces.event.RowEditEvent;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Date;
import java.util.Scanner;

@ManagedBean
@SessionScoped
public class MeteoDataController {

	@ManagedProperty("#{meteoDataService}")
	private MeteoDataService meteoDataService;

	public MeteoDataService getMeteoDataService() {
		return meteoDataService;
	}

	public void setMeteoDataService(MeteoDataService meteoDataService) {
		this.meteoDataService = meteoDataService;
	}

	@PostConstruct
	public void init() {
		listAccuWeather = meteoDataService.getAllMeteoDataAccuWeather();
		listGisMeteo = meteoDataService.getAllMeteoDataGisMeteo();
	}

	private Collection<MeteoData> listAccuWeather;
	private Collection<MeteoData> listGisMeteo;

	public Collection<MeteoData> getListAccuWeather() {
		return listAccuWeather;
	}

	public Collection<MeteoData> getListGisMeteo() {
		return listGisMeteo;
	}

	public void refreshAccuWeatherData() {
		meteoDataService.removeAllDataAccuWeather();
		MeteoData moscowMeteoData = getMoscowForecastAccuWeather();
		MeteoData permMeteoData = getPermForecastAccuWeather();
		MeteoData yekaterinburgData = getYekaterinburgForecastAccuWeather();
		meteoDataService.register(moscowMeteoData);
		meteoDataService.register(permMeteoData);
		meteoDataService.register(yekaterinburgData);
		init();
	}

	public void refreshGisMeteoData() {
		meteoDataService.removeAllDataGisMeteo();
		MeteoData moscowMeteoData = getMoscowForecastGisMeteo();
		MeteoData permMeteoData = getPermForecastGisMeteo();
		MeteoData yekaterinburgData = getYekaterinburgForecastGisMeteo();
		meteoDataService.register(moscowMeteoData);
		meteoDataService.register(permMeteoData);
		meteoDataService.register(yekaterinburgData);
		init();
	}

	private MeteoData getMoscowForecastAccuWeather() {
		MeteoData md = new MeteoData();
		md.setCity("Moscow");
		md.setRead_timestamp(new Date());

		ForecastIO fio = new ForecastIO(FIOApiKey.getApikey());
		fio.setUnits(ForecastIO.UNITS_SI);
		fio.setLang(ForecastIO.LANG_ENGLISH);
		fio.getForecast("55.7500" , "37.6167");

		FIOCurrently currently = new FIOCurrently(fio);

		md.setTemperature(BigDecimal.valueOf(currently.get().apparentTemperature()));
		md.setHumidity(currently.get().humidity().intValue());
		md.setPressure(currently.get().pressure().intValue());
		md.setWind_direction(currently.get().windBearing().intValue());
		md.setWind_speed(currently.get().windSpeed().intValue());
		return md;
	}

	private MeteoData getMoscowForecastGisMeteo() {
		MeteoData md = new MeteoData();
		md.setCity("Москва");
		md.setRead_timestamp(new Date());

		String content = null;
		URLConnection connection;
		try {
			connection =  new URL("http://www.gismeteo.ru/city/daily/4368/").openConnection();
			Scanner scanner = new Scanner(connection.getInputStream());
			scanner.useDelimiter("\\Z");
			content = scanner.next();
		}catch ( Exception ex ) {
			ex.printStackTrace();
		}
		if (content != null) {
			content = content.substring(content.indexOf("<div class=\"section higher\">"),content.indexOf("<!--block expire"));
			String[] lines = content.split("\\n");
			for (String line : lines) {

				try {
					line = new String(line.getBytes(),"UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}

				if (line.contains("<dd class='value m_temp c'>")) {
					if (line.contains("minus")) {
						md.setTemperature(BigDecimal.valueOf(-Integer.parseInt(line.replaceAll("[^\\d.]", ""))));
					}
					else {
						md.setTemperature(BigDecimal.valueOf(Integer.parseInt(line.replaceAll("[^\\d.]", ""))));
					}
				}


					if (line.contains("западный")) md.setWind_direction(180);
					if (line.contains("юго-западный")) md.setWind_direction(235);
					if (line.contains("северо-западный")) md.setWind_direction(135);
					if (line.contains("восточный")) md.setWind_direction(0);
					if (line.contains("юго-восточный")) md.setWind_direction(315);
					if (line.contains("северо-восточный")) md.setWind_direction(45);
					if (line.contains("южный")) md.setWind_direction(270);
					if (line.contains("северный")) md.setWind_direction(90);


				if (line.contains("value m_wind ms")) md.setWind_speed(Integer.parseInt(line.replaceAll("[^\\d]", "")));

				if (line.contains("value m_press torr")) md.setPressure(Integer.parseInt(line.replaceAll("[^\\d]", "")));

				if (line.contains("Влажность")) md.setHumidity(Integer.parseInt(line.replaceAll("[^\\d]", "")));
			}
		}

		return md;
	}


	private MeteoData getPermForecastGisMeteo() {
		MeteoData md = new MeteoData();
		md.setCity("Пермь");
		md.setRead_timestamp(new Date());

		String content = null;
		URLConnection connection;
		try {
			connection =  new URL("http://www.gismeteo.ru/city/daily/4476/").openConnection();
			Scanner scanner = new Scanner(connection.getInputStream());
			scanner.useDelimiter("\\Z");
			content = scanner.next();
		}catch ( Exception ex ) {
			ex.printStackTrace();
		}
		if (content != null) {
			content = content.substring(content.indexOf("<div class=\"section higher\">"),content.indexOf("<!--block expire"));
			String[] lines = content.split("\\n");
			for (String line : lines) {

				try {
					line = new String(line.getBytes(),"UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}

				if (line.contains("<dd class='value m_temp c'>")) {
					if (line.contains("minus")) {
						md.setTemperature(BigDecimal.valueOf(-Integer.parseInt(line.replaceAll("[^\\d.]", ""))));
					}
					else {
						md.setTemperature(BigDecimal.valueOf(Integer.parseInt(line.replaceAll("[^\\d.]", ""))));
					}
				}

					if (line.contains("западный")) md.setWind_direction(180);
					if (line.contains("юго-западный")) md.setWind_direction(235);
					if (line.contains("северо-западный")) md.setWind_direction(135);
					if (line.contains("восточный")) md.setWind_direction(0);
					if (line.contains("юго-восточный")) md.setWind_direction(315);
					if (line.contains("северо-восточный")) md.setWind_direction(45);
					if (line.contains("южный")) md.setWind_direction(270);
					if (line.contains("северный")) md.setWind_direction(90);

				if (line.contains("value m_wind ms")) md.setWind_speed(Integer.parseInt(line.replaceAll("[^\\d]", "")));

				if (line.contains("value m_press torr")) md.setPressure(Integer.parseInt(line.replaceAll("[^\\d]", "")));

				if (line.contains("Влажность")) md.setHumidity(Integer.parseInt(line.replaceAll("[^\\d]", "")));
			}
		}

		return md;
	}

	private MeteoData getPermForecastAccuWeather() {
		MeteoData md = new MeteoData();
		md.setCity("Perm");
		md.setRead_timestamp(new Date());

		ForecastIO fio = new ForecastIO(FIOApiKey.getApikey());
		fio.setUnits(ForecastIO.UNITS_SI);
		fio.setLang(ForecastIO.LANG_ENGLISH);
		fio.getForecast("58.0000" , "56.3167");

		FIOCurrently currently = new FIOCurrently(fio);

		md.setTemperature(BigDecimal.valueOf(currently.get().apparentTemperature()));
		md.setHumidity(currently.get().humidity().intValue());
		md.setPressure(currently.get().pressure().intValue());
		md.setWind_direction(currently.get().windBearing().intValue());
		md.setWind_speed(currently.get().windSpeed().intValue());
		return md;
	}

	private MeteoData getYekaterinburgForecastAccuWeather() {
		MeteoData md = new MeteoData();
		md.setCity("Yekaterinburg");
		md.setRead_timestamp(new Date());

		ForecastIO fio = new ForecastIO(FIOApiKey.getApikey());
		fio.setUnits(ForecastIO.UNITS_SI);
		fio.setLang(ForecastIO.LANG_ENGLISH);
		fio.getForecast("56.8333" , "60.5833");

		FIOCurrently currently = new FIOCurrently(fio);

		md.setTemperature(BigDecimal.valueOf(currently.get().apparentTemperature()));
		md.setHumidity(currently.get().humidity().intValue());
		md.setPressure(currently.get().pressure().intValue());
		md.setWind_direction(currently.get().windBearing().intValue());
		md.setWind_speed(currently.get().windSpeed().intValue());
		return md;
	}

	private MeteoData getYekaterinburgForecastGisMeteo() {
		MeteoData md = new MeteoData();
		md.setCity("Екатеринбург");
		md.setRead_timestamp(new Date());

		String content = null;
		URLConnection connection;
		try {
			connection =  new URL("http://www.gismeteo.ru/city/daily/4517/").openConnection();
			Scanner scanner = new Scanner(connection.getInputStream());
			scanner.useDelimiter("\\Z");
			content = scanner.next();
		}catch ( Exception ex ) {
			ex.printStackTrace();
		}
		if (content != null) {
			content = content.substring(content.indexOf("<div class=\"section higher\">"),content.indexOf("<!--block expire"));
			String[] lines = content.split("\\n");
			for (String line : lines) {

				try {
					line = new String(line.getBytes(),"UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}

				if (line.contains("<dd class='value m_temp c'>")) {
					if (line.contains("minus")) {
						md.setTemperature(BigDecimal.valueOf(-Integer.parseInt(line.replaceAll("[^\\d.]", ""))));
					}
					else {
						md.setTemperature(BigDecimal.valueOf(Integer.parseInt(line.replaceAll("[^\\d.]", ""))));
					}
				}


				if (line.contains("западный")) md.setWind_direction(180);
				if (line.contains("юго-западный")) md.setWind_direction(235);
				if (line.contains("северо-западный")) md.setWind_direction(135);
				if (line.contains("восточный")) md.setWind_direction(0);
				if (line.contains("юго-восточный")) md.setWind_direction(315);
				if (line.contains("северо-восточный")) md.setWind_direction(45);
				if (line.contains("южный")) md.setWind_direction(270);
				if (line.contains("северный")) md.setWind_direction(90);


				if (line.contains("value m_wind ms")) md.setWind_speed(Integer.parseInt(line.replaceAll("[^\\d]", "")));

				if (line.contains("value m_press torr")) md.setPressure(Integer.parseInt(line.replaceAll("[^\\d]", "")));

				if (line.contains("Влажность")) md.setHumidity(Integer.parseInt(line.replaceAll("[^\\d]", "")));
			}
		}

		return md;
	}


	//Web-CRUD
	public void onRowEdit(RowEditEvent event) {
		meteoDataService.update((MeteoData) event.getObject());
		FacesMessage msg = new FacesMessage("Метеоданные обновлены", "Метеозапись № " + String.valueOf(((MeteoData) event.getObject()).getId()));
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("Изменения отменены", "Метеозапись № " + String.valueOf(((MeteoData) event.getObject()).getId()));
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}


}
