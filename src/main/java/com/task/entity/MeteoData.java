package com.task.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(schema = "public", name = "meteo_data")
public class MeteoData {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	private Long id;
	@Column(name="city")
	private String city;
	@Column(name="read_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date read_timestamp;
	@Column(name="temperature")
	private BigDecimal temperature;
	@Column(name="humidity")
	private Integer humidity;
	@Column(name="pressure")
	private Integer pressure;
	@Column(name="wind_direction")
	private Integer wind_direction;
	@Column(name="wind_speed")
	private Integer wind_speed;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getRead_timestamp() {
		return read_timestamp;
	}

	public void setRead_timestamp(Date read_timestamp) {
		this.read_timestamp = read_timestamp;
	}

	public BigDecimal getTemperature() {
		return temperature;
	}

	public void setTemperature(BigDecimal temperature) {
		this.temperature = temperature;
	}

	public Integer getHumidity() {
		return humidity;
	}

	public void setHumidity(Integer humidity) {
		this.humidity = humidity;
	}

	public Integer getPressure() {
		return pressure;
	}

	public void setPressure(Integer pressure) {
		this.pressure = pressure;
	}

	public Integer getWind_direction() {
		return wind_direction;
	}

	public void setWind_direction(Integer wind_direction) {
		this.wind_direction = wind_direction;
	}

	public Integer getWind_speed() {
		return wind_speed;
	}

	public void setWind_speed(Integer wind_speed) {
		this.wind_speed = wind_speed;
	}
}
