CREATE TABLE public.meteo_data
(
    id SERIAL PRIMARY KEY NOT NULL,
    city TEXT NOT NULL,
    read_timestamp TIMESTAMP NOT NULL,
    temperature DECIMAL(4,1) NOT NULL,
    humidity INT,
    pressure INT,
    wind_direction INT,
    wind_speed INT
);
ALTER TABLE public.meteo_data
 ADD CONSTRAINT unique_id UNIQUE (id);